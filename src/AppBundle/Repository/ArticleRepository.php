<?php

namespace AppBundle\Repository;

/**
 * ArticleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ArticleRepository extends \Doctrine\ORM\EntityRepository {

    public function getArticleByIdWithJoin($id) {
        $qb = $this->createQueryBuilder("a"); //équivaut à select tous les objets articles et ensuite on veut faire une jointure
        $qb->leftJoin('a.image', 'i')//image correspond au $image dans la classe article
                ->addSelect('i')//on fait addSelect pour ajouter au requettes précédentes sinon avec select tout court elle écraserai les requettes précédentes
                ->leftJoin('a.commentaires', 'c')//image correspond au $image dans la classe article
                ->addSelect('c')
                ->leftJoin('a.tags', 't')//image correspond au $image dans la classe article
                ->addSelect('t') //on fait addSelect pour ajouter au requettes précédentes sinon avec select tout court elle écraserai les requettes précédentes
                ->where('a.id = ?1')//requete préparée
                ->setParameter(1, $id);
        $query = $qb->getQuery();
        $article = $query->getOneOrNullResult();
        return $article;
    }

    public function getTousArticle() {
        $qb = $this->createQueryBuilder("a"); //équivaut à select tous les objets articles et ensuite on veut faire une jointure
        $qb->leftJoin('a.image', 'i')//image correspond au $image dans la classe article
                ->addSelect('i')
                ->leftJoin('a.tags', 't')//image correspond au $image dans la classe article
                ->addSelect('t') //on fait addSelect pour ajouter au requettes précédentes sinon avec select tout court elle écraserai les requettes précédentes
                ->orderBy('a.date', 'DESC')
                ->where('a.publication = 1');
        $query = $qb->getQuery();
        $articles = $query->getResult(); //ou getArryResult()
        return $articles;
    }

}
