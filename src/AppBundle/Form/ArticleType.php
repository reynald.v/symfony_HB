<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('titre', TextType::class, ['label' => 'label du titre'])
                ->add('contenu')
                ->add('auteur')
                ->add('date')
                ->add('publication', CheckboxType::class, ['required' => false])
                //->add('enregistrer', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class)//taper submit , autocompletion et rajouter class
                ->add('image', ImageType::class)//fix uses//pour image on fait un form supplémentaire
                ->add('tags', EntityType::class, [
                    'required' => false,
                    'class' => \AppBundle\Entity\Tag::class,
                    'choice_label' => 'titre',
                    'expanded' => true, //false champs avec liste deroulante, true case à cocher
                    'multiple' => true, //false bouton radio, true case à cocher choix multiple
                    'query_builder' => function($er) {//fct anonyme pour classer par ordre alphabetique la class ci dessus Tag
                        $qb = $er->createQueryBuilder('t');
                        $qb->orderBy("t.titre", "ASC");
                        return $qb;
                    },
                ])//fix uses
                ->add('enregistrer', SubmitType::class)//fix uses
//                ->add('image')
//                ->add('tags')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Article'//liaison avec la classe Article
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_article';
    }

}
