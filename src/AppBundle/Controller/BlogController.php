<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/blog")
 */ //possibilité de préfixer par /blog dans app/config/routing.yml
class BlogController extends Controller {

    /**
     * This is the documentation description of your method, it will appear
     * on a specific pane. It will read all the text until the first
     * annotation.
     *
     * @ApiDoc(
     *  resource=true,
     *  description="This is a description of your API method",

     * )
     * @Route("/{p}", name="homepage_blog",
     * defaults={"p":1},
     * requirements={"p":"\d+"})//ici on spécifie que le parametre doit être un entier !
     * //{p} est un parametre supplémentaire d'url sur route avec sa valeur par défaut dans defaults
     */
    public function indexAction(Request $request, $p) {

        $ar = $this->getDoctrine()->getManager()->getRepository('AppBundle:Article');


//        $articles = $ar->findAll(); //équivalent fetchAll()
        //$articles = $ar->findBy(['publication' => true], ['date' => 'DESC', 'titre' => 'ASC']);
        $articles = $ar->getTousArticle();
        dump($articles);
//        $articles = [
//            [
//                'id' => 1,
//                'titre' => 'Hello world 1',
//                'contenu' => 'Lorem Ipsum <em>dolor11</em>',
//                'date' => new \Datetime()
//            ], [
//                'id' => 2,
//                'titre' => 'Hello world 2',
//                'contenu' => 'Lorem Ipsum22 <em>dolor</em>',
//                'date' => new \Datetime()
//            ], [
//                'id' => 3,
//                'titre' => 'Hello world3',
//                'contenu' => 'Lorem Ipsum333 <em>dolor</em>',
//                'date' => new \Datetime()
//        ]];
        return $this->render('blog/index.html.twig', ['page' => $p, 'articles' => $articles]); //nom de la vue doit etre la même que le nom de la méthode "index"Action + supprimer le tableau en argument + rajouter un tableau argument avec nos propres arguments , ici p
    }

    /**
     * @ApiDoc(
     *  description="ajoutAction"
     * )
     * @Route("/ajout/", name="ajout_blog")
     */
    public function ajoutAction(Request $request) {
        $article = new \AppBundle\Entity\Article;
        $form = $this->createForm(\AppBundle\Form\ArticleType::class, $article);

//        $article->setTitre('titre nouvel article')
//                ->setContenu('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget luctus elit. Aliquam ac est luctus odio tempor consectetur. Sed nunc elit, lacinia ut tempor a, lobortis sed ligula. Ut nec semper arcu. Mauris ac semper urna. Quisque non varius tortor, non finibus erat. Ut eget molestie turpis. Pellentesque ac lobortis lacus, non finibus velit. Sed volutpat sem risus, at luctus sem scelerisque sed.')
//                ->setAuteur('AuteurNouvelArticle');
//        $image = new \AppBundle\Entity\Image;
//        $image->setAlt('Roboash')
//                ->setUrl('https://robohash.org/' . rand() . '.png?set=set2');
//
//        $article->setImage($image);
//
//        $commentaire1 = new \AppBundle\Entity\Commentaire();
//        $commentaire2 = new \AppBundle\Entity\Commentaire();
//
//
//        $commentaire1->setArticle($article)
//                ->setContenu('um dolor sit amet, consectetur adipiscing elit. Integer eget luctus elit. Aliquam ac est luctus odio tempor consectetur. Sed nunc elit, lacinia ut tempor a, lobortis sed ligula. Ut nec semper arcu. Mauris ac semper urna. Quisque non varius tortor, non finibus erat. Ut eget molestie turpis. Pellentesque');
//
//        $commentaire2->setArticle($article)
//                ->setContenu('Consectetur adipiscing elit. Ut nec semper arcu. Mauris ac semper urna. Quisque non varius tortor, non finibus erat. Ut eget molestie turpis. Pellentesque');
//
//        $tag1 = new \AppBundle\Entity\Tag();
//        $tag2 = new \AppBundle\Entity\Tag();
//        $tag1->setTitre('symfony');
//        $tag2->setTitre('php');
//
//        $article->addTag($tag1)->addTag($tag2);
//        //$doctrine= $this->get('doctrine');//équivaut à la ligne ci dessous
////        $doctrine = $this->getDoctrine(); //méthode pour récuperer le service doctrine qui a une méthode getManager pour récuperer un entity manager
////        $em = $doctrine->getManager();
//        $em = $this->getDoctrine()->getManager(); //équivaut aux 2 lignes ci dessus
//
//        $em->persist($article); //méthode pour dire que l'objet article sera à traiter, persistera également l'image car on l'a mis en cascade dans les annotaions de la propriét $image de la class article
//        $em->persist($commentaire1);
//        $em->persist($commentaire2);
//
//        $em->flush(); //ouvre la transaction + persist les objets
//
//        return $this->redirectToRoute('detail_blog', ['id' => $article->getId()]);
        dump($form);
        $form->handleRequest($request); //1er chargement handle request fait rien et issubmitted retourne false
        if ($form->isSubmitted() && $form->isValid()) {//si le form est soumis is submitted retourne  vrai et is valid retourne faux en cas d'erreur
            $em = $this->getDoctrine()->getManager();
            $em->persist($article); //on a pas besoin de persister l'image car elle est en cascade
            //recuperer le  service session
            $session = $this->get('session');

            try {
                $em->flush();
                //recup un objet flashbag du servivce session, success est un tableau
                $session->getFlashBag()->add('success', 'Article enregistré :)');
                return $this->redirectToRoute('detail_blog', ['id' => $article->getId()]);
            } catch (\Exception $ex) {
                $session->getFlashBag()->add('erreur', 'Probleme enregistrement :(');
            }
        }
        return $this->render('blog/ajout.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @ApiDoc(
     *  description="detailAction"
     * )
     * @Route("/detail/{id}", name="detail_blog",
     * requirements={"id":"\d+"})
     */
    public function detailAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager(); //->getManager() facultatif
        $ar = $em->getRepository('AppBundle:Article'); //ar = article repository(manager) = recupere la class article repository avec ses méthodes
        $cr = $em->getRepository('AppBundle:Commentaire');
        //$article = $ar->find($id);
        $article = $ar->getArticleByIdWithJoin($id);
        //ar = article repository = recupere la class article repository (manager/ modele) avec ses méthodes
        $commentaires = $cr->findBy(['article' => $article], ['date' => 'DESC']);
//        $article = [
//            'id' => $id,
//            'titre' => 'Hello world',
//            'contenu' => 'Lorem Ipsum <em>dolor</em>',
//            'date' => new \Datetime(),
//        ];
        return $this->render('blog/detail.html.twig', ['article' => $article, 'commentaires' => $commentaires]);
    }

    /**
     * @ApiDoc(
     *  description="tagAction"
     * )
     * @Route("/tag/{id}", name="tag_blog",
     * requirements={"id":"\d+"})
     */
    public function tagAction(Request $request, $id) {
        $tm = $this->getDoctrine()->getManager()->getRepository('AppBundle:Tag'); //tr = article repository(manager) = recupere la class article repository avec ses méthodes

        $tag = $tm->find($id);
        return $this->render('blog/tag.html.twig', ['id' => $id, 'tag' => $tag]);
    }

    /**
     * @ApiDoc(
     *  description="updateAction"
     * )
     * @Route("/update/{id}", name="update_blog",
     * requirements={"id":"\d+"})
     */
    public function updateAction(Request $request, $id) {
        $ar = $this->getDoctrine()->getManager()->getRepository('AppBundle:Article');
        $article = $ar->find($id);
        $form = $this->createForm(\AppBundle\Form\ArticleType::class, $article);


        $form->handleRequest($request); //1er chargement handle request fait rien et issubmitted retourne false
        if ($form->isSubmitted() && $form->isValid()) {//si le form est soumis is submitted retourne  vrai et is valid retourne faux en cas d'erreur
            $em = $this->getDoctrine()->getManager();

            //recuperer le  service session
            $session = $this->get('session');
            try {
                $em->flush();
                //recup un objet flashbag du servivce session, success est un tableau
                $session->getFlashBag()->add('success', 'Article enregistré :)');
                return $this->redirectToRoute('detail_blog', ['id' => $article->getId()]);
            } catch (\Exception $ex) {
                $session->getFlashBag()->add('erreur', 'Probleme enregistrement :(');
            }
        }
        return $this->render('blog/update.html.twig', ['form' => $form->createView()]);
        //return $this->render('blog/update.html.twig', ['id' => $id]);
    }

    /**
     * @ApiDoc(
     *  description="deleteAction"
     * )
     * @Route("/delete/{id}", name="delete_blog",
     * requirements={"id":"\d+"})
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
//        $ar = $em->getRepository('AppBundle:Article');
//        $article = $ar->find($id);//les 2 lignes commentées ici équivalent à la ligne ci dessous
        $article = $em->getRepository('AppBundle:Article')->find($id);
        $em->remove($article);

        //recuperer le  service session
        $session = $this->get('session');
        try {
            $em->flush();
            //recup un objet flashbag du servivce session, success est un tableau
            $session->getFlashBag()->add('success', 'Article supprimé :)');
            $session->getFlashBag()->add('success', 'Bien, bien supprimé :)');
            return $this->redirectToRoute('homepage_blog');
        } catch (\Exception $ex) {
            $session->getFlashBag()->add('erreur', 'Probleme suppression :(');
            return $this->redirectToRoute('detail_blog', ['id' => $article->getId()]);
        }

        //return $this->render('blog/delete.html.twig', ['id' => $id]);
    }

    /**
     * @ApiDoc(
     *  description="derniersArticlesAction"
     * )
     * @Route("/delete/{id}", name="derniersArticles_blog",
     * requirements={"id":"\d+"})
     */
    public function derniersArticlesAction($nbArticles) {
        $ar = $this->getDoctrine()->getManager()->getRepository('AppBundle:Article');
//        $articles = $ar->findAll(); //équivalent fetchAll()
        $articles = $ar->findBy(['publication' => true], ['date' => 'DESC', 'titre' => 'ASC'], $nbArticles); //1er tableau WHERE, 2eme tableau ORDER BY, 3eme LIMIT
//        $articles = [
//            [
//                'id' => 1,
//                'titre' => 'Hello world 1',
//                'contenu' => 'Lorem Ipsum <em>dolor11</em>',
//                'date' => new \Datetime()
//            ], [
//                'id' => 2,
//                'titre' => 'Hello world 2',
//                'contenu' => 'Lorem Ipsum22 <em>dolor</em>',
//                'date' => new \Datetime()
//            ], [
//                'id' => 3,
//                'titre' => 'Hello world3',
//                'contenu' => 'Lorem Ipsum333 <em>dolor</em>',
//                'date' => new \Datetime()
//        ]];
        return $this->render('blog/derniersArticles.html.twig', ['articles' => $articles]);
    }

}
