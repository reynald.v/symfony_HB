<?php

namespace CatalogueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */ //possibilité de préfixer par /catalogue dans app/config/routing.yml
class CatalogueController extends Controller {

    /**
     * @Route("/catalogue", name="homepage_catalogue",
     *  defaults={"p":1},
     * requirements={"p":"\d+"})//ici on spécifie que le parametre doit être un entier !
     * //{p} est un parametre supplémentaire d'url sur route avec sa valeur par défaut dans defaults
     */
    public function indexAction(Request $request) {

        $cr = $this->getDoctrine()->getManager()->getRepository('CatalogueBundle:Categorie');
        $categories = $cr->findAll(); //équivalent fetchAll()
        //$produits = $ar->findBy(['date' => 'DESC', 'titre' => 'ASC']);
//        $produits = [
//            [
//                'id' => 1,
//                'titre' => 'Hello world 1',
//                'contenu' => 'Lorem Ipsum <em>dolor11</em>',
//            ], [
//                'id' => 2,
//                'titre' => 'Hello world 2',
//                'contenu' => 'Lorem Ipsum22 <em>dolor</em>',
//            ], [
//                'id' => 3,
//                'titre' => 'Hello world3',
//                'contenu' => 'Lorem Ipsum333 <em>dolor</em>',
//        ]];
        return $this->render('CatalogueBundle:catalogue:index.html.twig', ['categories' => $categories]); //nom de la vue doit etre la même que le nom de la méthode "index"Action + supprimer le tableau en argument + rajouter un tableau argument avec nos propres arguments , ici p
    }

    /**
     * @Route("/catalogue/categorie/{id}", name="categorie", requirements={"id":"\d+"})
     */
    public function categorieAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $ar = $em->getRepository('CatalogueBundle:Produit'); //ar = article repository = recupere la class article repository avec ses méthodes
        $produits = $ar->findBy(['categorie' => $id]);
//        $article = [
//            'id' => $id,
//            'titre' => 'Hello world',
//            'contenu' => 'Lorem Ipsum <em>dolor</em>',
//            'date' => new \Datetime(),
//        ];
        return $this->render('CatalogueBundle:catalogue:categorie.html.twig', ['produits' => $produits]);
    }

    /**
     * @Route("/ajout/", name="ajout_catalogue")
     */
    public function ajoutAction(Request $request) {
        $article = new \Catalogue\Bundle\Entity\Produit;
        $article->setTitre('titre nouvel article')
                ->setContenu('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget luctus elit. Aliquam ac est luctus odio tempor consectetur. Sed nunc elit, lacinia ut tempor a, lobortis sed ligula. Ut nec semper arcu. Mauris ac semper urna. Quisque non varius tortor, non finibus erat. Ut eget molestie turpis. Pellentesque ac lobortis lacus, non finibus velit. Sed volutpat sem risus, at luctus sem scelerisque sed.')
                ->setAuteur('AuteurNouvelArticle');
        $image = new \AppBundle\Entity\Image;
        $image->setAlt('Roboash')
                ->setUrl('https://robohash.org/' . rand() . '.png?set=set2');

        $produit->setImage($image);
        //$doctrine= $this->get('doctrine');//équivaut à la ligne ci dessous
//        $doctrine = $this->getDoctrine(); //méthode pour récuperer le service doctrine qui a une méthode getManager pour récuperer un entity manager
//        $em = $doctrine->getManager();
        $em = $this->getDoctrine()->getManager(); //équivaut aux 2 lignes ci dessus
        $em->persist($produit); //méthode pour dire que l'objet article sera à traiter, persistera également l'image car on l'a mis en cascade dans les annotaions de la propriét $image de la class article
        $em->flush(); //ouvre la transaction + persist les objets

        return $this->redirectToRoute('detail_catalogue', ['id' => $produit->getId()]);
        //return $this->render('blog/ajout.html.twig');
    }

    /**
     * @Route("/detail/{id}", name="detail_catalogue",
     * requirements={"id":"\d+"})
     */
    public function detailAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $ar = $em->getRepository('CatalogueBundle:Produit'); //ar = article repository = recupere la class article repository avec ses méthodes
        $produit = $ar->find($id);
//        $article = [
//            'id' => $id,
//            'titre' => 'Hello world',
//            'contenu' => 'Lorem Ipsum <em>dolor</em>',
//            'date' => new \Datetime(),
//        ];
        return $this->render('catalogue/detail.html.twig', ['produit' => $produit]);
    }

    /**
     * @Route("/update/{id}", name="update_catalogue",
     * requirements={"id":"\d+"})
     */
    public function updateAction(Request $request, $id) {
        return $this->render('catalogue/update.html.twig', ['id' => $id]);
    }

    /**
     * @Route("/delete/{id}", name="delete_catalogue",
     * requirements={"id":"\d+"})
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
//        $ar = $em->getRepository('AppBundle:Article');
//        $article = $ar->find($id);//les 2 lignes commentées ici équivalent à la ligne ci dessous
        $article = $em->getRepository('CatalogueBundle:Produit')->find($id);
        $em->remove($produit);

        //recuperer le  service session
        $session = $this->get('session');
        try {
            $em->flush();
            //recup un objet flashbag du servivce session, success est un tableau
            $session->getFlashBag()->add('success', 'Produit supprimé :)');
            $session->getFlashBag()->add('success', 'Bien, bien supprimé :)');
            return $this->redirectToRoute('homepage_catalogue');
        } catch (Exception $ex) {
            $session->getFlashBag()->add('erreur', 'Probleme suppression :(');
            return $this->redirectToRoute('detail_catalogue', ['id' => $produit->getId()]);
        }

        //return $this->render('blog/delete.html.twig', ['id' => $id]);
    }

    /**
     * @Route("/delete/{id}", name="derniersArticles_catalogue",
     * requirements={"id":"\d+"})
     */
    public function derniersArticlesAction($nbProduits) {
        $ar = $this->getDoctrine()->getManager()->getRepository('CatalogueBundle:Produit');
        $produits = $ar->findAll(); //équivalent fetchAll()
        // $produits = $ar->findBy(['publication' => true], ['date' => 'DESC', 'titre' => 'ASC']);
//        $articles = [
//            [
//                'id' => 1,
//                'titre' => 'Hello world 1',
//                'contenu' => 'Lorem Ipsum <em>dolor11</em>',
//                'date' => new \Datetime()
//            ], [
//                'id' => 2,
//                'titre' => 'Hello world 2',
//                'contenu' => 'Lorem Ipsum22 <em>dolor</em>',
//                'date' => new \Datetime()
//            ], [
//                'id' => 3,
//                'titre' => 'Hello world3',
//                'contenu' => 'Lorem Ipsum333 <em>dolor</em>',
//                'date' => new \Datetime()
//        ]];
        return $this->render('catalogue/derniersProduits.html.twig', ['produits' => $produits]);
    }

}
